﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;

namespace ParetoCalculation.Core.Tests
{
    [TestFixture]
    public class ParetoTests
    {
        [Test]
        public void Constructor_Test()
        {
            var point1 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 1", new Point2D(1, 2), OxyColors.Black,
                MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 2", new Point2D(2, 1), OxyColors.Black,
                MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoDominatedPoint("pareto point 3", new Point2D(2, 3), OxyColors.Black,
                MarkerType.Circle);
            var globalOptimumPoint = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 1), OxyColors.Black,
                MarkerType.Circle);
            var points = new ParetoPointBase[] { point1, point2, point3, globalOptimumPoint };

            var paretoFrontLine =
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot);
            var diagonalLine = ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash);
            var lines = new ParetoLineBase[] { paretoFrontLine, diagonalLine };
            var pareto = new Pareto(points, lines);

            Assert.AreEqual(points, pareto.Points);
            Assert.AreEqual(lines, pareto.Lines);
        }

        [Test]
        public void Constructor_ThrowArgumentNullException_ForNullArguments_Test()
        {
            var point1 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 1", new Point2D(1, 2), OxyColors.Black,
                MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 2", new Point2D(2, 1), OxyColors.Black,
                MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoDominatedPoint("pareto point 3", new Point2D(2, 3), OxyColors.Black,
                MarkerType.Circle);
            var globalOptimumPoint = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 1), OxyColors.Black,
                MarkerType.Circle);
            var points = new ParetoPointBase[] { point1, point2, point3, globalOptimumPoint };

            var paretoFrontLine =
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot);
            var diagonalLine = ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash);
            var lines = new ParetoLineBase[] { paretoFrontLine, diagonalLine };

            Assert.Throws<ArgumentNullException>(() => new Pareto(null, lines));
            Assert.Throws<ArgumentNullException>(() => new Pareto(points, null));
        }

        [Test]
        public void Equals_Test()
        {
            var point1 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 1", new Point2D(1, 2), OxyColors.Black,
                MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 2", new Point2D(2, 1), OxyColors.Black,
                MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoDominatedPoint("pareto point 3", new Point2D(2, 3), OxyColors.Black,
                MarkerType.Circle);
            var globalOptimumPoint = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 1), OxyColors.Black,
                MarkerType.Circle);
            var points = new ParetoPointBase[] { point1, point2, point3 };

            var paretoFrontLine =
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot);
            var diagonalLine = ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash);
            var paretoLine = ParetoLineFactory.CreateParetoLine("Pareto line",
                new[] { new Point2D(2, 1), new Point2D(2, 2) }, OxyColors.Black, LineStyle.Dash);
            var lines = new ParetoLineBase[] { paretoFrontLine, diagonalLine };

            var pareto = new Pareto(points, lines);
            var pareto2 = new Pareto(points, lines);
            var pareto3 = new Pareto(new ParetoPointBase[] { point1, point2, globalOptimumPoint }, lines);
            var pareto4 = new Pareto(points, new ParetoLineBase[] { paretoFrontLine, paretoLine });
            var pareto5 = new Pareto(new ParetoPointBase[] { point1, point2 }, lines);
            var pareto6 = new Pareto(points, new ParetoLineBase[] { paretoFrontLine, paretoLine, diagonalLine });

            Assert.AreEqual(pareto, pareto2);
            Assert.AreNotEqual(pareto, pareto3);
            Assert.AreNotEqual(pareto, pareto4);
            Assert.AreNotEqual(pareto, pareto5);
            Assert.AreNotEqual(pareto, pareto6);
            Assert.AreNotEqual(pareto, pareto6);
        }

        [Test]
        public void Equals_ReturnTrueIf_ParetoPointsHaveDifferentOrder_Test()
        {
            var point1 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 1", new Point2D(1, 2), OxyColors.Black,
                MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 2", new Point2D(2, 1), OxyColors.Black,
                MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoDominatedPoint("pareto point 3", new Point2D(2, 3), OxyColors.Black,
                MarkerType.Circle);
            var globalOptimumPoint = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 1), OxyColors.Black,
                MarkerType.Circle);
            var points = new ParetoPointBase[] { point1, point2, point3 };
            var points2 = new ParetoPointBase[] { point2, point3, point1 };

            var paretoFrontLine =
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot);
            var diagonalLine = ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash);
            var paretoLine = ParetoLineFactory.CreateParetoLine("Pareto line",
                new[] { new Point2D(2, 1), new Point2D(2, 2) }, OxyColors.Black, LineStyle.Dash);
            var lines = new ParetoLineBase[] { paretoFrontLine, diagonalLine, paretoLine };

            var pareto = new Pareto(points, lines);
            var pareto2 = new Pareto(points2, lines);

            Assert.AreEqual(pareto, pareto2);
        }

        [Test]
        public void Equals_ReturnTrueIf_ParetoLinesHaveDifferentOrder_Test()
        {
            var point1 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 1", new Point2D(1, 2), OxyColors.Black,
                MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 2", new Point2D(2, 1), OxyColors.Black,
                MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoDominatedPoint("pareto point 3", new Point2D(2, 3), OxyColors.Black,
                MarkerType.Circle);
            var globalOptimumPoint = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 1), OxyColors.Black,
                MarkerType.Circle);
            var points = new ParetoPointBase[] { point1, point2, point3 };

            var paretoFrontLine =
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot);
            var diagonalLine = ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash);
            var paretoLine = ParetoLineFactory.CreateParetoLine("Pareto line",
                new[] { new Point2D(2, 1), new Point2D(2, 2) }, OxyColors.Black, LineStyle.Dash);
            var lines = new ParetoLineBase[] { paretoFrontLine, diagonalLine, paretoLine };
            var lines2 = new ParetoLineBase[] { paretoLine, paretoFrontLine, diagonalLine };

            var pareto = new Pareto(points, lines);
            var pareto2 = new Pareto(points, lines2);

            Assert.AreEqual(pareto, pareto2);
        }

        [Test]
        public void Equals_ReturnTrueIf_ParetoLinesHaveDifferentOrder_AndAreDifferentObjects_Test()
        {
            var point1 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 1", new Point2D(1, 2), OxyColors.Black,
                MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 2", new Point2D(2, 1), OxyColors.Black,
                MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoDominatedPoint("pareto point 3", new Point2D(2, 3), OxyColors.Black,
                MarkerType.Circle);
            var globalOptimumPoint = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 1), OxyColors.Black,
                MarkerType.Circle);
            var points = new ParetoPointBase[] { point1, point2, point3 };

            var lines = new ParetoLineBase[]
            {
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot),
                ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash),
                ParetoLineFactory.CreateParetoLine("Pareto line",
                new[] { new Point2D(2, 1), new Point2D(2, 2) }, OxyColors.Black, LineStyle.Dash)
            };

            var lines2 = new ParetoLineBase[]
            {
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot),
                ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash),
                ParetoLineFactory.CreateParetoLine("Pareto line",
                new[] { new Point2D(2, 1), new Point2D(2, 2) }, OxyColors.Black, LineStyle.Dash)
            };

            var pareto = new Pareto(points, lines);
            var pareto2 = new Pareto(points, lines2);

            Assert.AreEqual(pareto, pareto2);
        }
    }
}
