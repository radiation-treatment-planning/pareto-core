﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;

namespace ParetoCalculation.Core.Tests
{
    [TestFixture]
    public class ParetoLineFactoryTests
    {
        [Test]
        public void CreateParetoLine_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };
            var paretoLine = ParetoLineFactory.CreateParetoLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);

            Assert.AreEqual("tag", paretoLine.Tag);
            Assert.AreEqual(points, paretoLine.Points);
            Assert.AreEqual("Pareto line", paretoLine.Description);
            Assert.AreEqual(OxyColors.AliceBlue, paretoLine.Color);
            Assert.AreEqual(LineStyle.Dash, paretoLine.LineStyle);
        }

        [Test]
        public void ParetoLine_Equals_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };
            var samePoints = new[] { new Point2D(1, 2), new Point2D(2, 3) };
            var samePointsDifferentOrder = new[] { new Point2D(2, 3), new Point2D(1, 2) };
            var paretoLine = ParetoLineFactory.CreateParetoLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoLineEqual = ParetoLineFactory.CreateParetoLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoLineEqual2 = ParetoLineFactory.CreateParetoLine("tag", samePoints, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoLine2 = ParetoLineFactory.CreateParetoLine("tag", new[] { new Point2D(1, 2), new Point2D(2, 4) },
                OxyColors.AliceBlue, LineStyle.Dash);
            var paretoLine3 = ParetoLineFactory.CreateParetoLine("tag",
                new[] { new Point2D(1, 2), new Point2D(2, 3), new Point2D(3, 4) }, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoLine4 = ParetoLineFactory.CreateParetoLine("tag2", points, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoLine5 = ParetoLineFactory.CreateParetoLine("tag", points, OxyColors.Black, LineStyle.Dash);
            var paretoLine6 = ParetoLineFactory.CreateParetoLine("tag", points, OxyColors.AliceBlue, LineStyle.Solid);
            var paretoLine7 = ParetoLineFactory.CreateParetoLine("tag", samePointsDifferentOrder, OxyColors.AliceBlue, LineStyle.Solid);

            Assert.AreEqual(paretoLine, paretoLineEqual);
            Assert.AreEqual(paretoLine, paretoLineEqual);
            Assert.AreNotEqual(paretoLine, paretoLine2);
            Assert.AreNotEqual(paretoLine, paretoLine3);
            Assert.AreNotEqual(paretoLine, paretoLine4);
            Assert.AreNotEqual(paretoLine, paretoLine5);
            Assert.AreNotEqual(paretoLine, paretoLine6);
            Assert.AreNotEqual(paretoLine, paretoLine7);
        }

        [Test]
        public void ParetoLine_ThorArgumentNullException_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };

            Assert.Throws<ArgumentNullException>(() => ParetoLineFactory.CreateParetoLine(null, points, OxyColors.AliceBlue, LineStyle.Dash));
            Assert.Throws<ArgumentNullException>(() => ParetoLineFactory.CreateParetoLine("tag", null, OxyColors.AliceBlue, LineStyle.Dash));
        }

        [Test]
        public void ParetoLine_ThorArgumentException_IfLessThanTwoPointsAsArgument_Test()
        {
            var points = new[] { new Point2D(1, 2) };

            Assert.Throws<ArgumentException>(() => ParetoLineFactory.CreateParetoLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash));
        }

        [Test]
        public void CreateParetoFrontLine_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };
            var paretoFrontLine = ParetoLineFactory.CreateParetoFrontLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);

            Assert.AreEqual("tag", paretoFrontLine.Tag);
            Assert.AreEqual(points, paretoFrontLine.Points);
            Assert.AreEqual("Pareto front line", paretoFrontLine.Description);
            Assert.AreEqual(OxyColors.AliceBlue, paretoFrontLine.Color);
            Assert.AreEqual(LineStyle.Dash, paretoFrontLine.LineStyle);
        }

        [Test]
        public void ParetoFrontLine_Equals_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };
            var paretoFrontLine = ParetoLineFactory.CreateParetoFrontLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoFrontLineEqual = ParetoLineFactory.CreateParetoFrontLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoFrontLine2 = ParetoLineFactory.CreateParetoFrontLine("tag", new[] { new Point2D(1, 2), new Point2D(2, 4) },
                OxyColors.AliceBlue, LineStyle.Dash);
            var paretoFrontLine3 = ParetoLineFactory.CreateParetoFrontLine("tag",
                new[] { new Point2D(1, 2), new Point2D(2, 3), new Point2D(3, 4) }, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoFrontLine4 = ParetoLineFactory.CreateParetoFrontLine("tag2", points, OxyColors.AliceBlue, LineStyle.Dash);
            var paretoFrontLine5 = ParetoLineFactory.CreateParetoFrontLine("tag", points, OxyColors.Black, LineStyle.Dash);
            var paretoFrontLine6 = ParetoLineFactory.CreateParetoFrontLine("tag", points, OxyColors.AliceBlue, LineStyle.Solid);

            Assert.AreEqual(paretoFrontLine, paretoFrontLineEqual);
            Assert.AreNotEqual(paretoFrontLine, paretoFrontLine2);
            Assert.AreNotEqual(paretoFrontLine, paretoFrontLine3);
            Assert.AreNotEqual(paretoFrontLine, paretoFrontLine4);
            Assert.AreNotEqual(paretoFrontLine, paretoFrontLine5);
            Assert.AreNotEqual(paretoFrontLine, paretoFrontLine6);
        }

        [Test]
        public void ParetoFrontLine_ThorArgumentNullException_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };

            Assert.Throws<ArgumentNullException>(() => ParetoLineFactory.CreateParetoFrontLine(null, points, OxyColors.AliceBlue, LineStyle.Dash));
            Assert.Throws<ArgumentNullException>(() => ParetoLineFactory.CreateParetoFrontLine("tag", null, OxyColors.AliceBlue, LineStyle.Dash));
        }

        [Test]
        public void ParetoFrontLine_ThorArgumentException_IfLessThanTwoPointsAsArgument_Test()
        {
            var points = new[] { new Point2D(1, 2) };

            Assert.Throws<ArgumentException>(() => ParetoLineFactory.CreateParetoFrontLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash));
        }

        [Test]
        public void CreateDiagonalLine_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };
            var diagonalLine = ParetoLineFactory.CreateDiagonalLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);

            Assert.AreEqual("tag", diagonalLine.Tag);
            Assert.AreEqual(points, diagonalLine.Points);
            Assert.AreEqual("Diagonal line", diagonalLine.Description);
            Assert.AreEqual(OxyColors.AliceBlue, diagonalLine.Color);
            Assert.AreEqual(LineStyle.Dash, diagonalLine.LineStyle);
        }

        [Test]
        public void DiagonalLine_Equals_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };
            var diagonalLine = ParetoLineFactory.CreateDiagonalLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);
            var diagonalLineEqual = ParetoLineFactory.CreateDiagonalLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash);
            var diagonalLine2 = ParetoLineFactory.CreateDiagonalLine("tag", new[] { new Point2D(1, 2), new Point2D(2, 4) },
                OxyColors.AliceBlue, LineStyle.Dash);
            var diagonalLine3 = ParetoLineFactory.CreateDiagonalLine("tag",
                new[] { new Point2D(1, 2), new Point2D(2, 3), new Point2D(3, 4) }, OxyColors.AliceBlue, LineStyle.Dash);
            var diagonalLine4 = ParetoLineFactory.CreateDiagonalLine("tag2", points, OxyColors.AliceBlue, LineStyle.Dash);
            var diagonalLine5 = ParetoLineFactory.CreateDiagonalLine("tag", points, OxyColors.Black, LineStyle.Dash);
            var diagonalLine6 = ParetoLineFactory.CreateDiagonalLine("tag", points, OxyColors.AliceBlue, LineStyle.Solid);

            Assert.AreEqual(diagonalLine, diagonalLineEqual);
            Assert.AreNotEqual(diagonalLine, diagonalLine2);
            Assert.AreNotEqual(diagonalLine, diagonalLine3);
            Assert.AreNotEqual(diagonalLine, diagonalLine4);
            Assert.AreNotEqual(diagonalLine, diagonalLine5);
            Assert.AreNotEqual(diagonalLine, diagonalLine6);
        }

        [Test]
        public void DiagonalLine_ThorArgumentNullException_Test()
        {
            var points = new[] { new Point2D(1, 2), new Point2D(2, 3) };

            Assert.Throws<ArgumentNullException>(() => ParetoLineFactory.CreateDiagonalLine(null, points, OxyColors.AliceBlue, LineStyle.Dash));
            Assert.Throws<ArgumentNullException>(() => ParetoLineFactory.CreateDiagonalLine("tag", null, OxyColors.AliceBlue, LineStyle.Dash));
        }

        [Test]
        public void DiagonalLine_ThorArgumentException_IfLessThanTwoPointsAsArgument_Test()
        {
            var points = new[] { new Point2D(1, 2) };

            Assert.Throws<ArgumentException>(() => ParetoLineFactory.CreateDiagonalLine("tag", points, OxyColors.AliceBlue, LineStyle.Dash));
        }
    }
}
