﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;

namespace ParetoCalculation.Core.Tests
{
    [TestFixture]
    public class ParetoPointFactoryTests
    {
        [Test]
        public void Constructors_ThrowArgumentNullException_Test()
        {
            var point = new Point2D(1.0, 2.0);
            var color = OxyColors.Black;
            var markerType = MarkerType.Circle;
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPointFactory.CreateParetoPoint(null, point, color, markerType));
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPointFactory.CreateParetoDominatedPoint(null, point, color, markerType));
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPointFactory.CreateParetoNonDominatedPoint(null, point, color, markerType));
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPointFactory.CreateIllusionPoint(null, point, color, markerType));
            Assert.Throws<ArgumentNullException>(() =>
                ParetoPointFactory.CreateGlobalOptimumPoint(null, point, color, markerType));
        }

        [Test]
        public void CreateParetoPoint_Test()
        {
            var point = ParetoPointFactory.CreateParetoPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);

            Assert.AreEqual("tag", point.Tag);
            Assert.AreEqual(Point2D.Origin, point.Point);
            Assert.AreEqual(OxyColors.Black, point.Color);
            Assert.AreEqual(MarkerType.Circle, point.MarkerType);
            Assert.AreEqual("Pareto point", point.Description);
            Assert.AreEqual(typeof(ParetoPoint), point.GetType());
        }

        [Test]
        public void CreateParetoNonDominatedPoint_Test()
        {
            var point = ParetoPointFactory.CreateParetoNonDominatedPoint("tag", Point2D.Origin, OxyColors.Black,
                MarkerType.Circle);

            Assert.AreEqual("tag", point.Tag);
            Assert.AreEqual(Point2D.Origin, point.Point);
            Assert.AreEqual(OxyColors.Black, point.Color);
            Assert.AreEqual(MarkerType.Circle, point.MarkerType);
            Assert.AreEqual("Pareto non dominated point", point.Description);
            Assert.AreEqual(typeof(ParetoNonDominatedPoint), point.GetType());
        }

        [Test]
        public void CreateParetoDominatedPoint_Test()
        {
            var point = ParetoPointFactory.CreateParetoDominatedPoint("tag", Point2D.Origin, OxyColors.Black,
                MarkerType.Circle);

            Assert.AreEqual("tag", point.Tag);
            Assert.AreEqual(Point2D.Origin, point.Point);
            Assert.AreEqual(OxyColors.Black, point.Color);
            Assert.AreEqual(MarkerType.Circle, point.MarkerType);
            Assert.AreEqual("Pareto dominated point", point.Description);
            Assert.AreEqual(typeof(ParetoDominatedPoint), point.GetType());
        }

        [Test]
        public void CreateGlobalOptimumPoint_Test()
        {
            var point = ParetoPointFactory.CreateGlobalOptimumPoint("tag", Point2D.Origin, OxyColors.Black,
                MarkerType.Circle);

            Assert.AreEqual("tag", point.Tag);
            Assert.AreEqual(Point2D.Origin, point.Point);
            Assert.AreEqual(OxyColors.Black, point.Color);
            Assert.AreEqual(MarkerType.Circle, point.MarkerType);
            Assert.AreEqual("Global optimum point", point.Description);
            Assert.AreEqual(typeof(GlobalOptimumPoint), point.GetType());
        }

        [Test]
        public void CreateIllusionPoint_Test()
        {
            var point = ParetoPointFactory.CreateIllusionPoint("tag", Point2D.Origin, OxyColors.Black,
                MarkerType.Circle);

            Assert.AreEqual("tag", point.Tag);
            Assert.AreEqual(Point2D.Origin, point.Point);
            Assert.AreEqual(OxyColors.Black, point.Color);
            Assert.AreEqual(MarkerType.Circle, point.MarkerType);
            Assert.AreEqual("Illusion point", point.Description);
            Assert.AreEqual(typeof(IllusionPoint), point.GetType());
        }

        [Test]
        public void ParetoPoint_Equals_Test()
        {
            var point = ParetoPointFactory.CreateParetoPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoPoint("tag2", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point4 = ParetoPointFactory.CreateParetoPoint("tag", new Point2D(1, 2), OxyColors.Black, MarkerType.Circle);
            var point5 = ParetoPointFactory.CreateParetoPoint("tag", Point2D.Origin, OxyColors.Yellow, MarkerType.Circle);
            var point6 = ParetoPointFactory.CreateParetoPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Cross);

            Assert.AreEqual(point, point2);
            Assert.AreNotEqual(point, point3);
            Assert.AreNotEqual(point, point4);
            Assert.AreNotEqual(point, point5);
            Assert.AreNotEqual(point, point6);
        }

        [Test]
        public void ParetoNonDominatedPoint_Equals_Test()
        {
            var point = ParetoPointFactory.CreateParetoNonDominatedPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoNonDominatedPoint("tag2", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point4 = ParetoPointFactory.CreateParetoNonDominatedPoint("tag", new Point2D(1, 2), OxyColors.Black, MarkerType.Circle);
            var point5 = ParetoPointFactory.CreateParetoNonDominatedPoint("tag", Point2D.Origin, OxyColors.Yellow, MarkerType.Circle);
            var point6 = ParetoPointFactory.CreateParetoNonDominatedPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Cross);

            Assert.AreEqual(point, point2);
            Assert.AreNotEqual(point, point3);
            Assert.AreNotEqual(point, point4);
            Assert.AreNotEqual(point, point5);
            Assert.AreNotEqual(point, point6);
        }

        [Test]
        public void ParetoDominatedPoint_Equals_Test()
        {
            var point = ParetoPointFactory.CreateParetoDominatedPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateParetoDominatedPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateParetoDominatedPoint("tag2", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point4 = ParetoPointFactory.CreateParetoDominatedPoint("tag", new Point2D(1, 2), OxyColors.Black, MarkerType.Circle);
            var point5 = ParetoPointFactory.CreateParetoDominatedPoint("tag", Point2D.Origin, OxyColors.Yellow, MarkerType.Circle);
            var point6 = ParetoPointFactory.CreateParetoDominatedPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Cross);

            Assert.AreEqual(point, point2);
            Assert.AreNotEqual(point, point3);
            Assert.AreNotEqual(point, point4);
            Assert.AreNotEqual(point, point5);
            Assert.AreNotEqual(point, point6);
        }

        [Test]
        public void IllusionPoint_Equals_Test()
        {
            var point = ParetoPointFactory.CreateIllusionPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateIllusionPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateIllusionPoint("tag2", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point4 = ParetoPointFactory.CreateIllusionPoint("tag", new Point2D(1, 2), OxyColors.Black, MarkerType.Circle);
            var point5 = ParetoPointFactory.CreateIllusionPoint("tag", Point2D.Origin, OxyColors.Yellow, MarkerType.Circle);
            var point6 = ParetoPointFactory.CreateIllusionPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Cross);

            Assert.AreEqual(point, point2);
            Assert.AreNotEqual(point, point3);
            Assert.AreNotEqual(point, point4);
            Assert.AreNotEqual(point, point5);
            Assert.AreNotEqual(point, point6);
        }

        [Test]
        public void GlobalOptimumPoint_Equals_Test()
        {
            var point = ParetoPointFactory.CreateGlobalOptimumPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point2 = ParetoPointFactory.CreateGlobalOptimumPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point3 = ParetoPointFactory.CreateGlobalOptimumPoint("tag2", Point2D.Origin, OxyColors.Black, MarkerType.Circle);
            var point4 = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 2), OxyColors.Black, MarkerType.Circle);
            var point5 = ParetoPointFactory.CreateGlobalOptimumPoint("tag", Point2D.Origin, OxyColors.Yellow, MarkerType.Circle);
            var point6 = ParetoPointFactory.CreateGlobalOptimumPoint("tag", Point2D.Origin, OxyColors.Black, MarkerType.Cross);

            Assert.AreEqual(point, point2);
            Assert.AreNotEqual(point, point3);
            Assert.AreNotEqual(point, point4);
            Assert.AreNotEqual(point, point5);
            Assert.AreNotEqual(point, point6);
        }
    }
}
