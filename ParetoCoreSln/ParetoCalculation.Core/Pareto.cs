﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ParetoCalculation.Core
{
    public class Pareto
    {
        public IEnumerable<ParetoPointBase> Points { get; }
        public IEnumerable<ParetoLineBase> Lines { get; }

        public Pareto(IEnumerable<ParetoPointBase> points, IEnumerable<ParetoLineBase> lines)
        {
            Points = points ?? throw new ArgumentNullException(nameof(points));
            Lines = lines ?? throw new ArgumentNullException(nameof(lines));
        }

        protected bool Equals(Pareto other)
        {
            var numberOfPoints = Points.Count();
            if (numberOfPoints != other.Points.Count()) return false;

            var numberOfLines = Lines.Count();
            if (numberOfLines != other.Lines.Count()) return false;

            // Check points.
            foreach (var point in Points)
                if (!other.Points.Contains(point))
                    return false;

            // Check lines.
            foreach (var line in Lines)
                if (!other.Lines.Contains(line))
                    return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            return Equals(obj as Pareto);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Points != null ? Points.GetHashCode() : 0) * 397) ^ (Lines != null ? Lines.GetHashCode() : 0);
            }
        }

        public static bool operator ==(Pareto left, Pareto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Pareto left, Pareto right)
        {
            return !Equals(left, right);
        }
    }
}
