﻿using System;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public abstract class ParetoPointBase
    {
        public object Tag { get; }
        public Point2D Point { get; }
        public OxyColor Color { get; }
        public MarkerType MarkerType { get; }
        public string Description { get; }

        protected ParetoPointBase(object tag, Point2D point, OxyColor color, MarkerType markerType, string description)
        {
            Tag = tag ?? throw new ArgumentNullException(nameof(tag));
            Point = point;
            Color = color;
            MarkerType = markerType;
            Description = description ?? throw new ArgumentNullException(nameof(description));
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            return Equals(obj as ParetoPointBase);
        }

        protected bool Equals(ParetoPointBase other)
        {
            if (GetType() != other.GetType()) return false;
            return Equals(Tag, other.Tag)
                   && Point.Equals(other.Point)
                   && Color.Equals(other.Color)
                   && MarkerType == other.MarkerType
                   && Description == other.Description;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Tag != null ? Tag.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Point.GetHashCode();
                hashCode = (hashCode * 397) ^ Color.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)MarkerType;
                hashCode = (hashCode * 397) ^ (Description != null ? Description.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
