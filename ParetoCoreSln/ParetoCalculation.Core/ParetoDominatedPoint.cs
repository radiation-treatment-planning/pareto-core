﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public class ParetoDominatedPoint : ParetoPointBase
    {
        protected internal ParetoDominatedPoint(object tag, Point2D point, OxyColor color, MarkerType markerType, string description)
            : base(tag, point, color, markerType, description)
        {
        }
    }
}
