﻿using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public class ParetoLine : ParetoLineBase
    {
        protected internal ParetoLine(object tag, IEnumerable<Point2D> points, OxyColor color, LineStyle lineStyle, string description)
            : base(tag, points, color, lineStyle, description)
        {
        }
    }
}
