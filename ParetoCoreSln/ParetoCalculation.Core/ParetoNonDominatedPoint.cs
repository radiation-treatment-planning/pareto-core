﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public class ParetoNonDominatedPoint : ParetoPointBase
    {
        protected internal ParetoNonDominatedPoint(object tag, Point2D point, OxyColor color, MarkerType markerType, string description)
            : base(tag, point, color, markerType, description)
        {
        }
    }
}
