﻿using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public class ParetoFrontLine : ParetoLineBase
    {
        protected internal ParetoFrontLine(object tag, IEnumerable<Point2D> points, OxyColor color, LineStyle lineStyle, string description)
            : base(tag, points, color, lineStyle, description)
        {
        }
    }
}
