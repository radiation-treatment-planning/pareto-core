﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public abstract class ParetoLineBase
    {
        public object Tag { get; }
        public IEnumerable<Point2D> Points { get; }
        public OxyColor Color { get; }
        public LineStyle LineStyle { get; }
        public string Description { get; }

        protected ParetoLineBase(object tag, IEnumerable<Point2D> points, OxyColor color, LineStyle lineStyle,
            string description)
        {
            if (description == null) throw new ArgumentNullException(nameof(description));
            Tag = tag ?? throw new ArgumentNullException(nameof(tag));
            Points = points ?? throw new ArgumentNullException(nameof(points));
            var numberOfPoints = points.Count();
            if (numberOfPoints < 2)
                throw new ArgumentException(
                    $"{nameof(points)} must contain at least two point, but contained {numberOfPoints}.");
            Color = color;
            LineStyle = lineStyle;
            Description = description;
        }

        public override bool Equals(object obj)
        {
            return obj != null && Equals(obj as ParetoLineBase);
        }

        protected bool Equals(ParetoLineBase other)
        {
            if (GetType() != other.GetType()) return false;
            var baseConditionsAreTrue = Equals(Tag, other.Tag)
                                        && Color.Equals(other.Color)
                                        && LineStyle == other.LineStyle
                                        && Description == other.Description;
            if (!baseConditionsAreTrue) return false;

            var points = Points.ToArray();
            var otherPoints = other.Points.ToArray();
            if (points.Length != otherPoints.Length) return false;
            var numberOfPoints = Points.Count();
            if (numberOfPoints != other.Points.Count()) return false;

            // Check whether other of points is also equal.
            for (var i = 0; i < points.Length; i++)
                if (!Equals(points[i], otherPoints[i]))
                    return false;

            return Equals(Tag, other.Tag)
                   && numberOfPoints == other.Points.Count()
                   && Color.Equals(other.Color)
                   && LineStyle == other.LineStyle
                   && Description == other.Description;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Tag != null ? Tag.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Points.GetHashCode();
                hashCode = (hashCode * 397) ^ Color.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)LineStyle;
                hashCode = (hashCode * 397) ^ (Description != null ? Description.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}
