﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public class IllusionPoint : ParetoPointBase
    {
        protected internal IllusionPoint(object tag, Point2D point, OxyColor color, MarkerType markerType, string description)
            : base(tag, point, color, markerType, description)
        {
        }
    }
}
