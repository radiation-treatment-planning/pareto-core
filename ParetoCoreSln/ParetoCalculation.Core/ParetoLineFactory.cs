﻿using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public class ParetoLineFactory
    {
        public static ParetoLine CreateParetoLine(object tag, IEnumerable<Point2D> points, OxyColor color, LineStyle lineStyle)
            => new ParetoLine(tag, points, color, lineStyle, "Pareto line");
        public static ParetoFrontLine CreateParetoFrontLine(object tag, IEnumerable<Point2D> points, OxyColor color, LineStyle lineStyle)
            => new ParetoFrontLine(tag, points, color, lineStyle, "Pareto front line");
        public static DiagonalLine CreateDiagonalLine(object tag, IEnumerable<Point2D> points, OxyColor color, LineStyle lineStyle)
            => new DiagonalLine(tag, points, color, lineStyle, "Diagonal line");
    }
}
