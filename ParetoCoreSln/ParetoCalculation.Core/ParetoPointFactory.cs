﻿using MathNet.Spatial.Euclidean;
using OxyPlot;

namespace ParetoCalculation.Core
{
    public class ParetoPointFactory
    {
        public static ParetoPoint CreateParetoPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            => new ParetoPoint(tag, point, color, markerType, "Pareto point");
        public static ParetoNonDominatedPoint CreateParetoNonDominatedPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            => new ParetoNonDominatedPoint(tag, point, color, markerType, "Pareto non dominated point");
        public static ParetoDominatedPoint CreateParetoDominatedPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            => new ParetoDominatedPoint(tag, point, color, markerType, "Pareto dominated point");
        public static IllusionPoint CreateIllusionPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            => new IllusionPoint(tag, point, color, markerType, "Illusion point");
        public static GlobalOptimumPoint CreateGlobalOptimumPoint(object tag, Point2D point, OxyColor color, MarkerType markerType)
            => new GlobalOptimumPoint(tag, point, color, markerType, "Global optimum point");
    }
}
