# ParetoCalculation.Core
A package with core dependencies for Pareto calculations.

## Installation
Build it from source or install it via [NuGet](https://www.nuget.org/packages/ParetoCalculation.Core/1.0.0).

## Usage
```csharp
// Create points.
var point1 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 1", new Point2D(1, 2), OxyColors.Black,
                MarkerType.Circle);
var point2 = ParetoPointFactory.CreateParetoNonDominatedPoint("pareto point 2", new Point2D(2, 1), OxyColors.Black,
                MarkerType.Circle);
var point3 = ParetoPointFactory.CreateParetoDominatedPoint("pareto point 3", new Point2D(2, 3), OxyColors.Black,
                MarkerType.Circle);
var globalOptimumPoint = ParetoPointFactory.CreateGlobalOptimumPoint("tag", new Point2D(1, 1), OxyColors.Black,
                MarkerType.Circle);
var points = new[] { point1, point2, point3, globalOptimumPoint };

// Create lines.
var paretoFrontLine =
                ParetoLineFactory.CreateParetoFrontLine("Pareto front line",
                    new[] { new Point2D(1, 2), new Point2D(2, 1) }, OxyColors.Red, LineStyle.Dot);
var diagonalLine = ParetoLineFactory.CreateDiagonalLine("Diagonal line",
                new[] { new Point2D(0, 0), new Point2D(3, 3) }, OxyColors.Black, LineStyle.Dash);
var lines = new[] { paretoFrontLine, diagonalLine };

// Create a pareto.
var pareto = new Pareto(points, lines);
```
